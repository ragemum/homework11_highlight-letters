const buttons = document.getElementsByClassName('btn');

document.addEventListener('keydown', function(event) {
    const { code } = event;

    for (let i = 0; i < buttons.length; i++) {
        const button = buttons[i];
        
        const { innerHTML } = button;
        if (innerHTML === code || `Key${innerHTML}` === code) {
            button.classList.add("selected");
        } else {
            button.classList.remove("selected");
        }
    }
});
